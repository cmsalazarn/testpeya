package com.cristhian.peya.controller;

import java.sql.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.cristhian.peya.model.Score;
import com.cristhian.peya.service.ScoreService;
import com.cristhian.peya.util.ActionResult;
import com.cristhian.peya.util.ActionResultPage;

@RestController
@RequestMapping("/api/v1")
public class ScoreController {
	
	@Autowired
	ScoreService scoreService;
	
	@GetMapping("score")
	public ActionResultPage<Score> getScores(@RequestParam(name="page") int page, 
			@RequestParam(name="size") int size) {
		return scoreService.getAllScore(page, size);
	}
	
	@GetMapping("score/{id}")
	public Score getScoreById(@PathVariable(name="id") Long id) {
		return scoreService.getScoreById(id);
	}
	
	@GetMapping("score/user/{idUser}")
	public ActionResultPage<Score> getScoreByUser(@PathVariable(name="idUser") Long idUser, 
			@RequestParam(name="dateStart") Date dateStart, 
			@RequestParam(name="dateEnd") Date dateEnd,
			@RequestParam(name="page") int page,
			@RequestParam(name="size") int size) {
		return scoreService.getScoreByUser(idUser, dateStart, dateEnd, page, size);
	}
	
	@GetMapping("score/store/{idStore}")
	public ActionResultPage<Score> getScoreByStore(@PathVariable(name="idStore") Long idStore, 
			@RequestParam(name="dateStart") Date dateStart, 
			@RequestParam(name="dateEnd") Date dateEnd,
			@RequestParam(name="page") int page,
			@RequestParam(name="size") int size) {
		return scoreService.getScoreByStore(idStore, dateStart, dateEnd, page, size);
	}
	
	@PostMapping("score")
	public ActionResult postScores(@RequestBody Score score) {
		return scoreService.newScore(score);
	}
	
	@PutMapping("score/{id}")
	public ActionResult putScore(@PathVariable(name="id") Long id, @RequestBody Score score) {
		return scoreService.editScore(id, score);
	}
	
	@DeleteMapping("score/{id}")
	public ActionResult deleteScore(@PathVariable(name="id") Long id) {
		return scoreService.deleteScore(id);
	}
	
}
