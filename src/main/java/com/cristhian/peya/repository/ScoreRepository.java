package com.cristhian.peya.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.cristhian.peya.model.Score;

@Repository
public interface ScoreRepository extends JpaRepository<Score, Long> {
	
	@Query(value = "select s from Score s where s.idPurchase = ?1")
	List<Score> getScoreByPurchase(Long idPurchase);
	
	@Query(value = "select s from Score s where s.isDeleted = 0 and s.idUser = ?1 and cast(s.creationDate as date) between ?2 and ?3 order by s.creationDate desc")
	Page<Score> getScoreByUser(Long idUser, Date start, Date end, Pageable pageable);
	
	@Query(value = "select s from Score s where s.isDeleted = 0 and s.idStore = ?1 and cast(s.creationDate as date) between ?2 and ?3 order by s.creationDate desc")
	Page<Score> getScoreByStore(Long idStore, Date start, Date end, Pageable pageable);
	
}
