package com.cristhian.peya.util;

import java.util.List;
import org.springframework.data.domain.Page;

public class ActionResultPage<T> {

	private List<T> data;
	private int pageNumber;
	private int totalPages;
	private Long totalElements;
	private int numberOfElements;
	
	public ActionResultPage(Page<T> page) {
		this.setData(page.getContent());
		this.setNumberOfElements(page.getNumberOfElements());
		this.setPageNumber(page.getNumber());
		this.setTotalElements(page.getTotalElements());
		this.setTotalPages(page.getTotalPages());
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public Long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Long totalElements) {
		this.totalElements = totalElements;
	}

	public int getNumberOfElements() {
		return numberOfElements;
	}

	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}
}
