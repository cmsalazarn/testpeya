package com.cristhian.peya.util;

public class ActionResult {

	private Boolean status;
	private String message;
	private Object data;
	
	public ActionResult(Boolean status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	
	public ActionResult(Boolean status, Object data) {
		super();
		this.status = status;
		this.data = data;
	}
	
	public ActionResult(Boolean status, String message, Object data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}
	public ActionResult() {
		super();
	}

	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
