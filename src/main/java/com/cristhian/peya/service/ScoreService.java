package com.cristhian.peya.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.cristhian.peya.model.Score;
import com.cristhian.peya.repository.ScoreRepository;
import com.cristhian.peya.util.ActionResult;
import com.cristhian.peya.util.ActionResultPage;

@Service
public class ScoreService {

	@Autowired
	ScoreRepository scoreRepo;
	
	public ActionResultPage<Score> getAllScore(int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return new ActionResultPage<Score>(scoreRepo.findAll(pageable));
	}
	
	public Score getScoreById(Long id) { 
		Optional<Score> score = scoreRepo.findById(id); 
		return score.orElse(new Score());
	}
	
	public ActionResultPage<Score> getScoreByUser(Long idUser, java.sql.Date dateStart, 
			java.sql.Date dateEnd, int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return new ActionResultPage<Score>(scoreRepo.getScoreByUser(idUser, dateStart, dateEnd, pageable));
	}
	
	public ActionResultPage<Score> getScoreByStore(Long idStore, java.sql.Date dateStart,
			java.sql.Date dateEnd, int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return new ActionResultPage<Score>(scoreRepo.getScoreByStore(idStore, dateStart, dateEnd, pageable));
	}
	
	public ActionResult newScore(Score score) {
		List<Score> listScore = scoreRepo.getScoreByPurchase(score.getIdPurchase());
		if (listScore.size() > 0) {
			return new ActionResult(false, "There is already a score for this purchase");
		}
		if (score.getScore() < 1 ||  score.getScore() > 5) {
			return new ActionResult(false, "The score should be between 1 and 5");
		}
		score.setCreationDate(new Date());
		score = scoreRepo.save(score);
		return new ActionResult(true, "Score saved correctly", score);
	}
	
	public ActionResult editScore(Long idScore, Score score) {
		Optional<Score> scoreResult = scoreRepo.findById(idScore);
		if (!scoreResult.isPresent())
			return new ActionResult(false, "The score does not exist");
		Score scoreSave = scoreResult.get();
		if (scoreSave.IsDeleted() == 1)
			return new ActionResult(false, "The score is already eliminated");
		if (score.getScore() < 1 ||  score.getScore() > 5) {
			return new ActionResult(false, "The score should be between 1 and 5");
		}
		scoreSave.setComment(score.getComment());
		scoreSave.setScore(score.getScore());
		scoreSave.setModificationDate(new Date());
		scoreSave = scoreRepo.save(scoreSave);
		return new ActionResult(true, "Score updated correctly", scoreSave);
	}
	
	public ActionResult deleteScore(Long id) {
		Optional<Score> scoreResult = scoreRepo.findById(id);
		if (!scoreResult.isPresent())
			return new ActionResult(false, "The score does not exist");
		Score score = scoreResult.get();
		if (score.IsDeleted() == 1)
			return new ActionResult(false, "The score is already eliminated");
		score.setIsDeleted(1);
		score.setDeleteDate(new Date());
		scoreRepo.save(score);
		return new ActionResult(true, "Score removed correctly");
	}
	
}
