package com.cristhian.peya.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "score")
public class Score {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private long idUser;
	private long idStore;
	private long idPurchase;
	private String comment;
	private int score;
	private int isDeleted;
	private Date creationDate;
	private Date modificationDate;
	private Date deleteDate;
	
	
	public Score(long id, long idUser, long idStore, long idPurchase, String comment, int score) {
		super();
		this.id = id;
		this.idUser = idUser;
		this.idStore = idStore;
		this.idPurchase = idPurchase;
		this.comment = comment;
		this.score = score;
	}
	public Score() {
		super();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdUser() {
		return idUser;
	}
	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}
	public long getIdStore() {
		return idStore;
	}
	public void setIdStore(long idStore) {
		this.idStore = idStore;
	}
	public long getIdPurchase() {
		return idPurchase;
	}
	public void setIdPurchase(long idPurchase) {
		this.idPurchase = idPurchase;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getModificationDate() {
		return modificationDate;
	}
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	public Date getDeleteDate() {
		return deleteDate;
	}
	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}
	public int IsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}
