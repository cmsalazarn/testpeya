package com.cristhian.peya;

import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import com.cristhian.peya.model.Score;
import com.cristhian.peya.repository.ScoreRepository;
import com.cristhian.peya.util.ActionResult;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PeyaApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PeyaApplicationTests {

	@MockBean
	private ScoreRepository repo;
	@Autowired
	private TestRestTemplate rest;
	@LocalServerPort
	private int port;
	
	private String getRootUrl() {
		return "http://localhost:" + port + "/api/v1";
	}
	
	@Test
	public void getAllScore() {
		Score score = new Score(100,15,23,14,"Great!",4);
		when(repo.findById(new Long(100))).thenReturn(Optional.of(score));
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<Score> response = rest.exchange(getRootUrl() + "/score/100", HttpMethod.GET, entity, Score.class);
		Assert.assertNotNull(response.getBody());
		Assert.assertEquals(response.getBody().getId(), 100);
		Assert.assertEquals(response.getBody().getComment(), "Great!");
	}
	
	@Test
	public void testCreateScoreErrorPurchase() {
		List<Score> listPurchase = new ArrayList<Score>();
		listPurchase.add(new Score(99,15,456,14,":(",4));
		when(repo.getScoreByPurchase(new Long(456))).thenReturn(listPurchase);
		Score score = new Score();
		score.setIdUser(999);
		score.setIdPurchase(456);
		score.setIdStore(999);
		score.setComment("Great food! Thanks :)");
		score.setScore(4);
		ResponseEntity<ActionResult> postResponse = rest.postForEntity(getRootUrl() + "/score", score, ActionResult.class);
		Assert.assertNotNull(postResponse.getBody());
		Assert.assertFalse(postResponse.getBody().getStatus());
		Assert.assertEquals(postResponse.getBody().getMessage(), "There is already a score for this purchase");
	}
	
	@Test
	public void testCreateScore() {
		Score score = new Score();
		score.setIdUser(98);
		score.setIdPurchase(45);
		score.setIdStore(78);
		score.setComment("Great food! Thanks :)");
		score.setScore(2);
		when(repo.save(score)).thenReturn(score);
		ResponseEntity<ActionResult> postResponse = rest.postForEntity(getRootUrl() + "/score", score, ActionResult.class);
		Assert.assertNotNull(postResponse.getBody());
		Assert.assertTrue(postResponse.getBody().getStatus());
		Assert.assertEquals(postResponse.getBody().getMessage(), "Score saved correctly");
	}
	
	@Test
	public void testUpdateErrorEliminated() {
		Score score = new Score(14,98,45,78,"Great food! Thanks :)",4);
		score.setIsDeleted(1);
		when(repo.findById(score.getId())).thenReturn(Optional.of(score));
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<Score> entity = new HttpEntity<Score>(score, headers);
		ResponseEntity<ActionResult> response = rest.exchange(getRootUrl() + "/score/" + String.valueOf(score.getId()),
				HttpMethod.PUT, entity, ActionResult.class);
		Assert.assertNotNull(response.getBody());
		Assert.assertFalse(response.getBody().getStatus());
		Assert.assertEquals(response.getBody().getMessage(), "The score is already eliminated");
	}
	
	@Test
	public void testUpdateErrorValueScore() {
		Score score = new Score(14,98,45,78,"Great food! Thanks :)",15);
		when(repo.findById(score.getId())).thenReturn(Optional.of(score));
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<Score> entity = new HttpEntity<Score>(score, headers);
		ResponseEntity<ActionResult> response = rest.exchange(getRootUrl() + "/score/" + String.valueOf(score.getId()),
				HttpMethod.PUT, entity, ActionResult.class);
		Assert.assertNotNull(response.getBody());
		Assert.assertFalse(response.getBody().getStatus());
		Assert.assertEquals(response.getBody().getMessage(), "The score should be between 1 and 5");
	}
	
	@Test
	public void testUpdate() {
		Score score = new Score(14,98,45,78,"Great food! Thanks :)",4);
		when(repo.findById(score.getId())).thenReturn(Optional.of(score));
		score.setScore(2);
		when(repo.save(score)).thenReturn(score);
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<Score> entity = new HttpEntity<Score>(score, headers);
		ResponseEntity<ActionResult> response = rest.exchange(getRootUrl() + "/score/" + String.valueOf(score.getId()),
				HttpMethod.PUT, entity, ActionResult.class);
		Assert.assertNotNull(response.getBody());
		Assert.assertTrue(response.getBody().getStatus());
		Assert.assertEquals(response.getBody().getMessage(), "Score updated correctly");
	}
	
	@Test
	public void testDeleteError() {
		Score score = new Score(41,98,45,78,"Great food! Thanks :)",4);
		score.setIsDeleted(1);
		when(repo.findById(score.getId())).thenReturn(Optional.of(score));
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<ActionResult> response = rest.exchange(getRootUrl() + "/score/" + String.valueOf(score.getId()),
				HttpMethod.DELETE, entity, ActionResult.class);
		Assert.assertNotNull(response.getBody());
		Assert.assertFalse(response.getBody().getStatus());
		Assert.assertEquals(response.getBody().getMessage(), "The score is already eliminated");
	}
	
	@Test
	public void testDelete() {
		Score score = new Score(41,98,45,78,"Great food! Thanks :)",4);
		when(repo.findById(score.getId())).thenReturn(Optional.of(score));
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<Score> entity = new HttpEntity<Score>(score, headers);
		ResponseEntity<ActionResult> response = rest.exchange(getRootUrl() + "/score/" + String.valueOf(score.getId()),
				HttpMethod.DELETE, entity, ActionResult.class);
		Assert.assertNotNull(response.getBody());
		Assert.assertTrue(response.getBody().getStatus());
		Assert.assertEquals(response.getBody().getMessage(), "Score removed correctly");
	}

}

